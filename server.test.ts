import * as request from 'supertest'
import App from './app'

describe('Test the root path', () => {
  test('It should response the GET method', async (done) => {
    const response = await request(App.app).get('/');
    expect(response.statusCode).toBe(200);
    done();
  });
});